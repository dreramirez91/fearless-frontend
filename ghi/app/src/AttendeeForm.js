import React, { useState, useEffect } from 'react'

export default function AttendeeForm(props) {
    const [conferences, setConferences] = useState([])
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [conference, setConference] = useState('')
    const [formSuccess, setFormSuccess] = useState(false)
    const [loadSuccess, setLoadSuccess] = useState(false)


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/'

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data["conferences"])
            setLoadSuccess(true)
        }
    }
    function handleNameChange(e) {
        setName(e.target.value)
    }
    function handleEmailChange(e) {
        setEmail(e.target.value)
    }
    function handleConferenceChange(e) {
        setConference(e.target.value)
    }
    useEffect(() => {
        fetchData();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault()
        const data = {}
        data.name = name
        data.email = email
        data.conference = conference
        console.log(data)
        const attendeeUrl = "http://localhost:8001/api/attendees/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(attendeeUrl, fetchConfig)
        if (response.ok) {
            const newLocation = await response.json()
            console.log(newLocation)
            setName('')
            setEmail('')
            setConference('')
            setFormSuccess(true)
        }
    }
    return (<div className="container">
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            {!formSuccess &&
                                <form onSubmit={handleSubmit} id="create-attendee-form">
                                    <h1 className="card-title">It's Conference Time!</h1>
                                    <p className="mb-3">
                                        Please choose which conference
                                        you'd like to attend.
                                    </p>
                                    {!loadSuccess &&
                                        <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                                            <div className="spinner-grow text-secondary" role="status">
                                                <span className="visually-hidden">Loading...</span>
                                            </div>

                                        </div>
                                    }
                                    {loadSuccess &&
                                    <>
                                    <div className="mb-3">
                                        <select value={conference.href} onChange={handleConferenceChange} name="conference" id="conference" className="form-select" required>
                                            <option value="">Choose a conference</option>
                                            {conferences.map(conference => {
                                                return (<option value={conference.href} key={conference.href}>{conference.name}</option>)
                                            })}
                                        </select>
                                    </div>
                                    <p className="mb-3">
                                        Now, tell us about yourself.
                                    </p>
                                    <div className="row">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input value={name} onChange={handleNameChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                                                <label htmlFor="name">Your full name</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input value={email} onChange={handleEmailChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                                                <label htmlFor="email">Your email address</label>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="btn btn-lg btn-primary">I'm going!</button>
                                    </>
                                        }
                                </form>
                            }
                            {
                                formSuccess &&
                                <div className="alert alert-success mb-0" id="success-message">
                                    Congratulations! You're all signed up!
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>)

}
