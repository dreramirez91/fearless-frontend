import React, { useEffect, useState } from 'react';

export default function PresentationForm(props) {
const [conferences, setConferences] = useState([])
    const [presenterName, setPresenterName] = useState('')
    const [email, setEmail] = useState('')
    const [companyName, setCompanyName] = useState('')
    const [title, setTitle] = useState('')
    const [synopsis, setSynopsis] = useState('')
    const [conference, setConference] = useState('')


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data["conferences"])
        }
    }
    function handlePresenterNameChange(e) {
        setPresenterName(e.target.value)
    }
    function handleEmailChange(e) {
        setEmail(e.target.value)
    }
    function handleCompanyNameChange(e) {
        setCompanyName(e.target.value)
    }
    function handleTitleChange(e) {
        setTitle(e.target.value)
    }
    function handleSynopsisChange(e) {
        setSynopsis(e.target.value)
    }
    function handleConferenceChange(e) {
        setConference(e.target.value)
    }
    useEffect(() => {
        fetchData();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault()
        const data = {}
        data.presenter_name = presenterName
        data.company_name = companyName
        data.presenter_email = email
        data.title = title
        data.synopsis = synopsis
        data.conference = conference
        console.log(data)
        const presentationUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(presentationUrl, fetchConfig)
        if (response.ok) {
            const newPresentation = await response.json()
            console.log(newPresentation)
            setPresenterName('')
            setCompanyName('')
            setEmail('')
            setTitle('')
            setSynopsis('')
            setConference('')
        }
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input name={presenterName} onChange={handlePresenterNameChange} placeholder="Presenter name" required type="text" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input name={email} onChange={handleEmailChange} placeholder="Presenter email" required type="text" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input name={companyName} onChange={handleCompanyNameChange} placeholder="Company name" required type="text" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input name={title} onChange={handleTitleChange} placeholder="Title" required type="text" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div>
                <label htmlFor="synopsis">Synopsis</label>
                <textarea name={synopsis} onChange={handleSynopsisChange} placeholder="" required type="text" id="city" className="form-control"></textarea>
              </div>
              <br/>
              <div className="mb-3">
                <select value={conference} onChange={handleConferenceChange} name="conference" required id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option value={conference.id} key={conference.id}>
                            {conference.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}
