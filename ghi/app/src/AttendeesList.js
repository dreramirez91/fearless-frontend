import React, { useState, useEffect } from 'react'

export default await function AttendeesList(props) { 
  const [attendees, setAttendees] = useState([])
  async function fetchAttendees() {
    const response = await fetch('http://localhost:8001/api/attendees/')
    if (response.ok) {
      const data = await response.json()
      console.log(data)
      setAttendees(data.attendees)
    }
  }
useEffect(() => {
  fetchAttendees()
}, [])

    return (
    <table className="table table-striped">
        <thead>
          <tr>
            <th className="w-50">Name</th>
            <th className="w-50">Conference</th>
          </tr>
        </thead>
        <tbody>
          {attendees.map(attendee => {
            return (
              <tr key={attendee.href}>
                <td>{ attendee.name }</td>
                <td> { attendee.conference }</td>
              </tr>
            )
          })}
        </tbody>
      </table>
)}
