import React, { useEffect, useState } from 'react';

export default function ConferenceForm(props) {
    const [locations, setLocations] = useState([])
    const [name, setName] = useState('')
    const [start, setStart] = useState('')
    const [end, setEnd] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentations] = useState('')
    const [maxAttendees, setMaxAttendees] = useState('')
    const [location, setLocation] = useState('')


    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data["locations"])
        }
    }
    function handleNameChange(e) {
        setName(e.target.value)
    }
    function handleStartChange(e) {
        setStart(e.target.value)
    }
    function handleEndChange(e) {
        setEnd(e.target.value)
    }
    function handleDescriptionChange(e) {
        setDescription(e.target.value)
    }
    function handleMaxPresentationsChange(e) {
        setMaxPresentations(e.target.value)
    }
    function handleMaxAttendeesChange(e) {
        setMaxAttendees(e.target.value)
    }
    function handleLocationChange(e) {
        setLocation(e.target.value)
    }
    useEffect(() => {
        fetchData();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault()
        const data = {}
        data.name = name
        data.starts = start
        data.ends = end
        data.description = description
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.location = location
        console.log(data)
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(conferenceUrl, fetchConfig)
        if (response.ok) {
            const newConference = await response.json()
            console.log(newConference)
            setName('')
            setStart('')
            setEnd('')
            setDescription('')
            setMaxPresentations('')
            setMaxAttendees('')
            setLocation('')
        }
    }
    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={start} onChange={handleStartChange} placeholder="Starts" required type="date" name="start" id="start" className="form-control" />
                            <label htmlFor="start">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={end} onChange={handleEndChange} placeholder="Ends" required type="date" name="end" id="end" className="form-control" />
                            <label htmlFor="city">Ends</label>
                        </div>
                        <div>
                            <label htmlFor="description">Description</label>
                            <textarea value={description} onChange={handleDescriptionChange} name="description" placeholder="Description" required type="text" id="description" className="form-control"></textarea>
                        </div>
                        <br />
                        <div className="form-floating mb-3">
                            <input value={maxPresentations} onChange={handleMaxPresentationsChange} name="maxPresentations" placeholder="Maximum presentations" required type="number" id="maxPresentations" className="form-control" />
                            <label htmlFor="maxPresentations">Maximum presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={maxAttendees} onChange={handleMaxAttendeesChange} name="maxAttendees" placeholder="Maximum attendees" required type="number" id="maxAttendees" className="form-control" />
                            <label htmlFor="maxAttendees">Maximum attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option value={location.id} key={location.id}>
                                            {location.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
