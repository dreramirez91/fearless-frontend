# Generated by Django 4.0.3 on 2023-05-24 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_create_states'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='picture_url',
            field=models.URLField(blank=True, null=True),
        ),
    ]
